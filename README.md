#Assigment for Timewax

* This is a simple API for a hypothetical e-commerce platform in which a few things is possible:
1. One can add, read, update items to and from the inventory
2. Items can be added or removed from a shopping cart

# Getting Started

* This project is made with the use of spring-boot (2.5.2), maven, docker, and postgres

1. Install maven on you machine - set the env var
2. install docker   
2. cd `/project_folder`
3. run `docker-compose up`
4. run `mvn clean install`
5. run `mvn spring-boot:run`

### Some major missing part from top of my mind
* Adding Error handling to the API and proper response messages on the controller layer
* Adding documentation either with Swagger of Spring REST Docs
* Adding security to the API for calling endpoints
* Adding user management
* Writing all the test for controller, service and repo layer
* Proper validation

