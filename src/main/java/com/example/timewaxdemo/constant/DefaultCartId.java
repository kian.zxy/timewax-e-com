package com.example.timewaxdemo.constant;

import java.util.UUID;

public class DefaultCartId {
    // this class should not be instantiated
    private DefaultCartId() {
        throw new IllegalStateException("Utility class");
    }

    public static final UUID CART_ID = UUID.fromString("d94eb3d1-102b-4368-86db-bbd516664074");
}
