package com.example.timewaxdemo.controller;

import com.example.timewaxdemo.model.Commodity;
import com.example.timewaxdemo.model.dto.CommodityDto;
import com.example.timewaxdemo.service.CommodityService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/commodity")
@RequiredArgsConstructor
public class CommodityController {

    private final ModelMapper modelMapper;

    private final CommodityService commodityService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public CommodityDto createCommodity(@RequestBody @Valid CommodityDto commodityDto) {
        Commodity commodity = convertToEntity(commodityDto);
        Commodity commodityCreated = commodityService.create(commodity);
        return modelMapper.map(commodityCreated, CommodityDto.class);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public CommodityDto updateCommodity(@RequestBody @Valid CommodityDto commodityDto) {
        Commodity commodityCreated = commodityService.update(commodityDto);
        return modelMapper.map(commodityCreated, CommodityDto.class);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<CommodityDto> getCommodities(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "5") int size
    ) {
        Page<Commodity> commodities = commodityService.getAllCommodities(page, size);
        return commodities.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }


    private Commodity convertToEntity(@NotNull CommodityDto commodityCreationDto) {
        Commodity commodity = modelMapper.map(commodityCreationDto, Commodity.class);
        commodity.setSKU(commodityCreationDto.updateSKU());
        return commodity;
    }

    private CommodityDto convertToDto(@NotNull Commodity commodity) {
        return modelMapper.map(commodity, CommodityDto.class);
    }

}
