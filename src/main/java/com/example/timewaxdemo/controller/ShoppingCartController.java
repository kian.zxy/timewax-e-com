package com.example.timewaxdemo.controller;

import com.example.timewaxdemo.model.Commodity;
import com.example.timewaxdemo.model.ShoppingCart;
import com.example.timewaxdemo.model.dto.ShoppingCartDto;
import com.example.timewaxdemo.service.CommodityService;
import com.example.timewaxdemo.service.ShoppingCartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(path = "/cart")
@RequiredArgsConstructor
public class ShoppingCartController {

    private final ModelMapper modelMapper;
    private final ShoppingCartService shoppingCartService;
    private final CommodityService commodityService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ShoppingCartDto getCart() {
        ShoppingCart shoppingCart = shoppingCartService.getTheCart();
        return convertToDto(shoppingCart);
    }

    @PostMapping(path = "/commodity/{id}/{count}")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public ShoppingCartDto setItem(@PathVariable UUID id, @PathVariable int count) {
        Optional<Commodity> commodity = commodityService.findById(id);
        ShoppingCart updatedShoppingCart = shoppingCartService.addItem(id, count, commodity.get());
        return convertToDto(updatedShoppingCart);
    }

    @PostMapping(path = "/coupon/{code}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public ShoppingCartDto applyCoupon(@PathVariable String code) {
        ShoppingCart updatedShoppingCart = shoppingCartService.applyCoupon(code);
        return convertToDto(updatedShoppingCart);
    }


    @DeleteMapping(path = "/commodity/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public ShoppingCartDto deleteItem(@PathVariable UUID id) {
        ShoppingCart updatedShoppingCart = shoppingCartService.deleteItem(id);
        return convertToDto(updatedShoppingCart);
    }

    @DeleteMapping(path = "/commodity")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public ShoppingCartDto deleteAllItems() {
        ShoppingCart updatedShoppingCart = shoppingCartService.deleteAllItems();
        return convertToDto(updatedShoppingCart);
    }

    private ShoppingCartDto convertToDto(@NotNull ShoppingCart shoppingCart) {
        return modelMapper.map(shoppingCart, ShoppingCartDto.class);
    }
}
