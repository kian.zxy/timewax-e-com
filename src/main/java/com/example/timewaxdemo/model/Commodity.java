package com.example.timewaxdemo.model;

import com.example.timewaxdemo.shared.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "commodity")
@EntityListeners(AuditingEntityListener.class)
public class Commodity extends AbstractEntity implements Serializable {

    private String SKU;

    @Column(unique = true) /* For not adding the SKU mapping table */
    private String displayName;

    @Column(columnDefinition = "text")
    private String description;

    private Double unitPrice;
    private Double quantity;

    @Override
    public String toString() {
        return "Commodity{" +
                "SKU='" + SKU + '\'' +
                ", displayName='" + displayName + '\'' +
                ", description='" + description + '\'' +
                ", unitPrice=" + unitPrice +
                ", quantity=" + quantity +
                '}';
    }
}
