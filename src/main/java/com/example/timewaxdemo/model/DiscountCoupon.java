package com.example.timewaxdemo.model;

import com.example.timewaxdemo.shared.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "discount_coupon")
@EntityListeners(AuditingEntityListener.class)
public class DiscountCoupon extends AbstractEntity implements Serializable {

    private String code;
    private Double discount;
    private boolean isUsed;

    @Override
    public String toString() {
        return "DiscountCoupon{" +
                "code='" + code + '\'' +
                ", discount=" + discount +
                ", isUsed=" + isUsed +
                '}';
    }
}
