package com.example.timewaxdemo.model;

import com.example.timewaxdemo.shared.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "shopping_cart")
@EntityListeners(AuditingEntityListener.class)
public class ShoppingCart extends AbstractEntity implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ShoppingCartItem> shoppingCartItems;

    @OneToOne
    private DiscountCoupon discountCoupon;

    private Double totalPrice;

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "shoppingCartItems=" + shoppingCartItems +
                ", discountCoupon=" + discountCoupon +
                ", totalPrice=" + totalPrice +
                '}';
    }

    public void calculateTotalPrice() {
        double total = 0d;
        Double discount;
        if (this.shoppingCartItems != null && !this.shoppingCartItems.isEmpty()) {
            for (ShoppingCartItem shoppingCartItem : this.shoppingCartItems) {
                total = total + (shoppingCartItem.getCommodity().getUnitPrice()) * shoppingCartItem.getCount();
            }

            if (discountCoupon != null && discountCoupon.getDiscount() > 0d && (total > 0d)) {
                discount = Math.floor(discountCoupon.getDiscount());
                total = Math.ceil((total * discount) / 100d);
            }
        }
        this.totalPrice = total;
    }
}
