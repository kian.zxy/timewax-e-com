package com.example.timewaxdemo.model;

import com.example.timewaxdemo.shared.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "cart_item")
@EntityListeners(AuditingEntityListener.class)
public class ShoppingCartItem extends AbstractEntity implements Serializable {

    @OneToOne
    private Commodity commodity;

    @Max(value=1000)
    @Min(value=1)
    private int count;

    @Override
    public String toString() {
        return "ShoppingCartItem{" +
                "commodity=" + commodity +
                ", count=" + count +
                '}';
    }

    public ShoppingCartItem(Commodity commodity, int count) {
        this.commodity = commodity;
        this.count = count;
    }
}
