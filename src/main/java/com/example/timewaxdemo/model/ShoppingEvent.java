package com.example.timewaxdemo.model;

import com.example.timewaxdemo.shared.AbstractEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "shopping_event")
@EntityListeners(AuditingEntityListener.class)
public class ShoppingEvent extends AbstractEntity implements Serializable {

    @Enumerated(EnumType.STRING)
    private Action action;

    @Column(columnDefinition = "text")
    private String payload;

    public ShoppingEvent(String payload, Action action) {
        this.payload = payload;
        this.action = action;
    }

    public enum Action {
        ADD,
        DELETE_ALL,
        DELETE_ONE,
        UPDATE
    }
}
