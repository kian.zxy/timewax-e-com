package com.example.timewaxdemo.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
public class CommodityDto {

    private UUID id;

    private String SKU;

    @NotNull
    private String displayName;

    private String description;

    @NotNull
    private Double unitPrice;

    @NotNull
    private Double quantity;

    public String updateSKU() {
        return this.displayName
                .replace(' ', '-')
                .replaceAll("([A-Z])", "$1")
                .toLowerCase();
    }

    @Override
    public String toString() {
        return "CommodityDto{" +
                "id=" + id +
                ", SKU='" + SKU + '\'' +
                ", displayName='" + displayName + '\'' +
                ", description='" + description + '\'' +
                ", unitPrice=" + unitPrice +
                ", quantity=" + quantity +
                '}';
    }
}
