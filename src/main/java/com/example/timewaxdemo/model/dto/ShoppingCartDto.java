package com.example.timewaxdemo.model.dto;

import com.example.timewaxdemo.model.DiscountCoupon;
import com.example.timewaxdemo.model.ShoppingCartItem;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ShoppingCartDto {

    private Set<ShoppingCartItem> shoppingCartItems;
    private DiscountCoupon discountCoupon;
    private Double totalPrice;
}
