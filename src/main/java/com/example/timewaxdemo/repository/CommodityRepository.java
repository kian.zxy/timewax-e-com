package com.example.timewaxdemo.repository;

import com.example.timewaxdemo.model.Commodity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CommodityRepository
        extends JpaRepository<Commodity, UUID>
{
    @Override
    Page<Commodity> findAll(Pageable pageable);
}
