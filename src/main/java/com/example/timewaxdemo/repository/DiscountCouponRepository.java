package com.example.timewaxdemo.repository;

import com.example.timewaxdemo.model.DiscountCoupon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface DiscountCouponRepository
        extends JpaRepository<DiscountCoupon, UUID> {

    DiscountCoupon getByCode(String code);
}
