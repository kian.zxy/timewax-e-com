package com.example.timewaxdemo.repository;

import com.example.timewaxdemo.model.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ShoppingCartRepository
        extends JpaRepository<ShoppingCart, UUID> {

    @Override
    List<ShoppingCart> findAll();
}
