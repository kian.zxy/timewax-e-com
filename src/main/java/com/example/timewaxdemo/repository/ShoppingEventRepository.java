package com.example.timewaxdemo.repository;

import com.example.timewaxdemo.model.ShoppingEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ShoppingEventRepository
        extends JpaRepository<ShoppingEvent, UUID> {
}
