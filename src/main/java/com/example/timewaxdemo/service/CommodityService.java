package com.example.timewaxdemo.service;

import com.example.timewaxdemo.model.Commodity;
import com.example.timewaxdemo.model.dto.CommodityDto;
import com.example.timewaxdemo.repository.CommodityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CommodityService {

    private final CommodityRepository commodityRepository;

    @Transactional
    public Commodity create(Commodity commodity) {
        return commodityRepository.save(commodity);
    }

    @Transactional
    public Commodity update(CommodityDto commodityDto) {
        Optional<Commodity> commodity = findById(commodityDto.getId());
        if (commodity.isEmpty()) {
            return null;
        }
        updateCommodityByDto(commodityDto, commodity.get());
        return commodityRepository.save(commodity.get());
    }

    public Page<Commodity> getAllCommodities(int page, int size) {
        Pageable paging = PageRequest.of(page, size);
        return commodityRepository.findAll(paging);
    }

    public Optional<Commodity> findById(UUID id) {
        if (commodityRepository.existsById(id)) {
            return commodityRepository.findById(id);
        } else {
            return Optional.empty();
        }
    }

    private void updateCommodityByDto(CommodityDto commodityDto, Commodity commodity) {
        commodity.setDescription(commodityDto.getDescription());
        commodity.setSKU(commodityDto.updateSKU());
        commodity.setDisplayName(commodityDto.getDisplayName());
        commodity.setQuantity(commodityDto.getQuantity());
        commodity.setUnitPrice(commodityDto.getUnitPrice());
    }
}
