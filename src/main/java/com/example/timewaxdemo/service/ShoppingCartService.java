package com.example.timewaxdemo.service;

import com.example.timewaxdemo.model.*;
import com.example.timewaxdemo.repository.DiscountCouponRepository;
import com.example.timewaxdemo.repository.ShoppingCartRepository;
import com.example.timewaxdemo.repository.ShoppingEventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.timewaxdemo.constant.DefaultCartId;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final DiscountCouponRepository discountCouponRepository;
    private final ShoppingEventRepository shoppingEventRepository;

    @Transactional
    public ShoppingCart getTheCart() {
        return shoppingCartRepository.getById(DefaultCartId.CART_ID);
    }

    @Transactional
    public ShoppingCart save(ShoppingCart shoppingCart) {
        return shoppingCartRepository.save(shoppingCart);
    }

    public DiscountCoupon getCouponByCode(String code) {
        return discountCouponRepository.getByCode(code);
    }

    public void saveEvents(String payload, ShoppingEvent.Action action) {
        ShoppingEvent shoppingEvent = new ShoppingEvent(payload, action);
        shoppingEventRepository.save(shoppingEvent);
    }

    // The below methods require clean up - code duplication
    public ShoppingCart deleteAllItems() {
        ShoppingCart shoppingCart = getTheCart();
        shoppingCart.setShoppingCartItems(null);
        shoppingCart.calculateTotalPrice();
        ShoppingCart updatedShoppingCart = save(shoppingCart);
        saveEvents(updatedShoppingCart.toString(), ShoppingEvent.Action.DELETE_ALL);
        return updatedShoppingCart;
    }

    public ShoppingCart deleteItem(UUID id) {
        ShoppingCart shoppingCart = getTheCart();
        shoppingCart.getShoppingCartItems().removeIf(s -> s.getCommodity().getId().compareTo(id) == 0);
        shoppingCart.calculateTotalPrice();
        ShoppingCart updatedShoppingCart = save(shoppingCart);
        saveEvents(updatedShoppingCart.toString(), ShoppingEvent.Action.DELETE_ONE);
        return updatedShoppingCart;
    }

    public ShoppingCart applyCoupon(String code) {
        ShoppingCart shoppingCart = getTheCart();
        DiscountCoupon discountCoupon = getCouponByCode(code);
        if (!discountCoupon.isUsed()) {
            shoppingCart.setDiscountCoupon(discountCoupon);
            shoppingCart.calculateTotalPrice();
        }
        ShoppingCart updatedShoppingCart = save(shoppingCart);
        saveEvents(updatedShoppingCart.toString(), ShoppingEvent.Action.UPDATE);
        return updatedShoppingCart;
    }

    public ShoppingCart addItem(UUID id, int count, Commodity commodity) {
        ShoppingCartItem shoppingCartItem = new ShoppingCartItem(commodity, count);
        ShoppingCart shoppingCart = getTheCart();

        shoppingCart.getShoppingCartItems().removeIf(s -> s.getCommodity().getId().compareTo(id) == 0);
        shoppingCart.getShoppingCartItems().add(shoppingCartItem);

        shoppingCart.calculateTotalPrice();
        ShoppingCart updatedShoppingCart = save(shoppingCart);
        saveEvents(updatedShoppingCart.toString(), ShoppingEvent.Action.ADD);
        return updatedShoppingCart;
    }
}
