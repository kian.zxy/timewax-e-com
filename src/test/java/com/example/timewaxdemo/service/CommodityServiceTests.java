package com.example.timewaxdemo.service;

import com.example.timewaxdemo.model.Commodity;
import com.example.timewaxdemo.repository.CommodityRepository;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@ExtendWith(SpringExtension.class)
public class CommodityServiceTests {

    @Autowired
    private CommodityRepository commodityRepository;

    @Test
    public void testGetAllCommodities() throws Exception {
        Pageable paging = PageRequest.of(0, 5);
        Page<Commodity> commodities = commodityRepository.findAll(paging);
        BDDAssertions.then(commodities.getTotalElements()).isEqualTo(3);
    }
}
