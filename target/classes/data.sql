INSERT INTO commodity (ID, revision, created_date, last_modified_date, SKU, DISPLAY_NAME, UNIT_PRICE, quantity, description)
VALUES('d94eb3d1-102b-4368-86db-bbd519974074', '0', current_timestamp, current_timestamp, 'bag', 'Bag', '10' , '222', 'brown');

INSERT INTO commodity (ID, revision, created_date, last_modified_date, SKU, DISPLAY_NAME, UNIT_PRICE, quantity, description)
VALUES('d94eb3d1-102b-4368-86db-aad519974074', '0', current_timestamp, current_timestamp, 'chair', 'Chair', '1000' , '400', 'blue');

INSERT INTO commodity (ID, revision, created_date, last_modified_date, SKU, DISPLAY_NAME, UNIT_PRICE, quantity, description)
VALUES('d94eb3d1-102b-4368-86db-ccd519974074', '0', current_timestamp, current_timestamp, 'table', 'Table', '11200' , '51', 'red');

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd518884074', 'nFtTXzaziK', current_timestamp, current_timestamp, '10', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd515554074', 'O3gdvTOAvo', current_timestamp, current_timestamp, '15', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd519994074', 'HLC4Azq0KC', current_timestamp, current_timestamp, '20', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd517774074', 'YGqfVBXaSI', current_timestamp, current_timestamp, '25', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd516664074', 'yT7yndEgld', current_timestamp, current_timestamp, '30', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd512224074', 'valid', current_timestamp, current_timestamp, '30', '0', false);

INSERT INTO discount_coupon (ID, code, created_date, last_modified_date, discount, revision, is_used)
VALUES('d94eb3d1-102b-4368-86db-bbd513334074', 'invalid', current_timestamp, current_timestamp, '30', '0', true);

INSERT INTO shopping_cart (ID, created_date, last_modified_date, revision)
VALUES('d94eb3d1-102b-4368-86db-bbd516664074', current_timestamp, current_timestamp , '0');
